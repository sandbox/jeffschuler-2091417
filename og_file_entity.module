<?php

/**
 * @file
 * Provide Organic Group -based permissions for managing file entities.
 */

/**
 * Implements hook_og_permission().
 */
function og_file_entity_og_permission() {
  $permissions = array(
    'edit any file in group' => array(
      'title' => t('Edit any file in group'),
    ),
    'delete any file in group' => array(
      'title' => t('Delete any file in group'),
    ),
  );

  return $permissions;
}

/**
 * Implements hook_file_entity_access().
 */
function og_file_entity_file_entity_access($op, $file, $account) {
  $file_gids = array();
  // Get all groups the file is part of.
  if ($file_entity_groups = og_get_entity_groups('file', $file)) {
    $file_gids = array_values($file_entity_groups['node']);
  }

  if ($op == 'update') {
    foreach ($file_gids as $gid) {
      if (og_user_access('node', $gid, 'edit any file in group')) {
        return FILE_ENTITY_ACCESS_ALLOW;
      }
    }
  }

  if ($op == 'delete') {
    foreach ($file_gids as $gid) {
      if (og_user_access('node', $gid, 'delete any file in group')) {
        return FILE_ENTITY_ACCESS_ALLOW;
      }
    }
  }

  // Otherwise, don't affect file access.
  return FILE_ENTITY_ACCESS_IGNORE;
}